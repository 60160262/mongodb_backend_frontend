var express = require('express')
var router = express.Router()

/* GET users listing. */
router.get('/:message', (req, res, next) => {
  const { params } = req
  res.json({ message: 'Hello Kob', params })
})

module.exports = router
