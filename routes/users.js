var express = require('express')
var router = express.Router()
const userControlloer = require('../controlloer/UserControlloer')
const User =require('../model/User')
/* GET users listing. */
router.get('/:id', userControlloer.getUser)

router.get('/',userControlloer.getUser)

router.post('/',userControlloer.addUser)

router.put('/:id',userControlloer.updateUser)

router.delete('/:id',userControlloer.deleteUser)
module.exports = router
